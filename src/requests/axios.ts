import axios from "axios";

const http = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 30000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

// Custom AXios settings
/*
http.interceptors.request.use(
  (config: any) => {
    const accessToken = localStorage.getItem("access");

    if (accessToken) {
      config.headers["Authorization"] = "Bearer " + accessToken;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

http.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (error?.response?.status === 401 && !originalRequest?._retry) {
      const refresh = localStorage.getItem("refresh");
      originalRequest._retry = true;
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_API_URL}/api/token/refresh/`,
          { refresh }
        );
        localStorage.setItem("refresh", res.data.refresh);
        localStorage.setItem("access", res.data.access);
        axios.defaults.headers.common["Authorization"] =
          "Bearer " + res.data.access;
      } catch (error) {
        localStorage.removeItem("refresh");
        localStorage.removeItem("access");
        localStorage.removeItem("data");
        window.location.href = "/sign-in";
      }
      return http(originalRequest);
    }
    return Promise.reject(error);
  }
);
*/

export default http;
