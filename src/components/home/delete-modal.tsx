import { useMutation, useQueryClient } from "@tanstack/react-query";
import Button from "@mui/material/Button";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import { Alert } from "@mui/material";
import { deleteUser } from "src/requests";

const DeleteModal = ({ user, open, onClose }: any) => {
  const queryClient = useQueryClient();
  const deleteUserMutation = useMutation({
    mutationFn: deleteUser,
    onSuccess: (data) => {
      queryClient.setQueryData(["users", data?.id], data);
      queryClient.invalidateQueries(["users"], { exact: true });
      onClose();
    },
  });

  function handleDelete(id: string) {
    deleteUserMutation.mutate(id);
  }

  return (
    <Dialog
      sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 450 } }}
      maxWidth="xs"
      open={open}
      onClose={onClose}
    >
      <DialogTitle>حذف ردیف</DialogTitle>
      <DialogContent>
        <span>آیا از حذف ردیف مطمين هستید؟</span>
        {deleteUserMutation.isError ? (
          <Alert severity="error">خطایی رخ داده</Alert>
        ) : null}
      </DialogContent>
      <DialogActions sx={{ p: 2 }}>
        <Button onClick={() => handleDelete(user?.id)} variant="contained">
          حذف
        </Button>
        <Button onClick={onClose} variant="outlined">
          بستن
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteModal;
