export interface IUser {
  id: string;
  fullName: string;
  mobile: string;
  age: string;
  email: string;
}
