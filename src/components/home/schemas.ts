import Joi from "joi";

export const schema = Joi.object({
  fullName: Joi.string().required().messages({
    "any.required": "نام الزامی است",
    "string.empty": "نام الزامی است",
  }),
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required()
    .messages({
      "string.empty": "ایمیل الزامی است",
      "string.email": "فرمت ایمیل اشتباه است",
    }),
  mobile: Joi.string()
    .required()
    .regex(/^[0-9]*$/)
    .length(11)
    .messages({
      "any.required": "شماره موبایل الزامی است",
      "string.empty": "شماره موبایل الزامی است",
      "string.pattern.base": `شماره موبایل باید عدد و ۱۱ رقمی باشد`,
      "string.base": `شماره موبایل باید عدد باشد`,
      "string.length": "شماره موبایل باید عدد و ۱۱ رقمی باشد",
    }),
  age: Joi.number().required().messages({
    "any.required": "سن الزامی است",
    "number.pattern.base": `سن باید عدد باشد`,
    "number.base": `سن باید عدد باشد`,
  }),
});
