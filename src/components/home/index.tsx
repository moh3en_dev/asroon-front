import { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { DeleteForever, EditOutlined } from "@mui/icons-material";
import { getUsers } from "src/requests";
import { IUser } from "src/models/user";
import ItemModal from "./item-modal";
import DeleteModal from "./delete-modal";
import RtlWrapper from "../common/rtl-wrapper";
import Loading from "../common/loading";

function Home() {
  const [selectedUser, setSelectedUser] = useState<IUser | null>(null);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [itemModalOpen, setItemModalOpen] = useState(false);
  const {
    isLoading,
    error,
    data = [],
  } = useQuery({
    queryKey: ["users"],
    queryFn: getUsers,
  });

  function renderTable() {
    if (isLoading) return <Loading />;
    if (error) return <p>خطایی رخ داده</p>;
    return (
      <TableContainer component={Paper}>
        <Table aria-label="user table">
          <TableHead>
            <TableRow>
              <TableCell>ردیف</TableCell>
              <TableCell>نام و نام خانوادگی</TableCell>
              <TableCell>شماره موبایل</TableCell>
              <TableCell>سن</TableCell>
              <TableCell>ایمیل</TableCell>
              <TableCell>عملیات</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((user: IUser, idx: number) => (
              <TableRow key={user?.id}>
                <TableCell>{idx + 1}</TableCell>
                <TableCell>{user?.fullName}</TableCell>
                <TableCell>{user?.mobile}</TableCell>
                <TableCell>{user?.age}</TableCell>
                <TableCell>{user?.email}</TableCell>
                <TableCell>
                  <EditOutlined
                    onClick={() => {
                      setSelectedUser(user);
                      setItemModalOpen(true);
                    }}
                    sx={{ cursor: "pointer", mx: 1 }}
                  />
                  <DeleteForever
                    onClick={() => {
                      setSelectedUser(user);
                      setDeleteModalOpen(true);
                    }}
                    color="primary"
                    sx={{ cursor: "pointer", mx: 1 }}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return (
    <RtlWrapper>
      <Container maxWidth="lg">
        <Box sx={{ display: "flex", justifyContent: "flex-end", my: 4 }}>
          <Button
            variant="contained"
            onClick={() => {
              setSelectedUser(null);
              setItemModalOpen(true);
            }}
          >
            + ایجاد داده جدید
          </Button>
        </Box>
        <Box sx={{ my: 4 }}>
          {renderTable()}
          {deleteModalOpen && (
            <DeleteModal
              user={selectedUser}
              open={deleteModalOpen}
              onClose={() => setDeleteModalOpen(false)}
            />
          )}
          {itemModalOpen && (
            <ItemModal
              user={selectedUser}
              open={itemModalOpen}
              onClose={() => setItemModalOpen(false)}
            />
          )}
        </Box>
      </Container>
    </RtlWrapper>
  );
}

export default Home;
