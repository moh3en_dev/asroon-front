import { IUser } from "src/models/user";
import API from "src/utils/APIs";
import http from "./axios";

export const getUsers = async () => {
  const { data } = await http.get<IUser[]>(API.users.list);
  return data;
};

export const getUser = async (id: string) => {
  const { data } = await http.get<IUser>(API.users.item(id));
  return data;
};

export const addUser = async (body: IUser) => {
  const { data } = await http.post<IUser>(API.users.list, body);
  return data;
};

export const updateUser = async (body: IUser) => {
  const { data } = await http.put<IUser>(API.users.item(body.id), body);
  return data;
};

export const deleteUser = async (id: string) => {
  const { data } = await http.delete<IUser>(API.users.item(id));
  return data;
};
