// @ts-nocheck
import React from "react";
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";

// TO-DO
// OPEN ISSUE: https://github.com/styled-components/stylis-plugin-rtl/issues/33
const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [rtlPlugin],
});

const rtlTheme = createTheme({
  direction: "rtl",
  typography: {
    fontFamily: "Shabnam",
  },
  palette: {
    primary: {
      light: "#757ce8",
      main: "#de2d26",
      dark: "#f44336",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#f44336",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});

// @ts-ignore
const RtlWrapper = ({ children }) => {
  React.useLayoutEffect(() => {
    document.body.setAttribute("dir", "rtl");
  }, []);
  return (
    <CacheProvider value={cacheRtl}>
      <ThemeProvider theme={rtlTheme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </CacheProvider>
  );
};

export default RtlWrapper;
