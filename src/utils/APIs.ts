/* eslint-disable import/no-anonymous-default-export */
export default {
  users: {
    list: `users/`,
    item: (id: string) => `users/${id}`,
  },
};
