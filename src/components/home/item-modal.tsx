import { useEffect } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import { Alert, Box, TextField } from "@mui/material";
import Button from "@mui/material/Button";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import { IUser } from "src/models/user";
import { addUser, updateUser } from "src/requests";
import { schema } from "./schemas";

const ItemModal = ({ user, open, onClose }: any) => {
  const queryClient = useQueryClient();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<IUser>({
    defaultValues: user,
    resolver: joiResolver(schema),
  });

  const addUserMutation = useMutation({
    mutationFn: addUser,
    onSuccess: (data) => {
      queryClient.setQueryData(["users", data?.id], data);
      queryClient.invalidateQueries(["users"], { exact: true });
    },
  });

  const updateUserMutation = useMutation({
    mutationFn: updateUser,
    onSuccess: (data) => {
      queryClient.setQueryData(["users", data?.id], data);
      queryClient.invalidateQueries(["users"], { exact: true });
    },
  });

  const onSubmit: SubmitHandler<IUser> = (values) => {
    if (user) {
      values.id = user.id;
      updateUserMutation.mutate(values);
    } else {
      addUserMutation.mutate(values);
    }
    onClose();
  };

  useEffect(() => {
    if (user && open) {
      const values = {
        fullName: user.fullName,
        mobile: user.mobile,
        age: user.age,
        email: user.email,
      };
      reset(values);
    } else {
      reset({
        fullName: "",
        age: undefined,
        email: "",
        mobile: "",
      });
    }
    return () => {};
  }, [reset, user, open]);

  return (
    <Dialog
      keepMounted
      sx={{ "& .MuiDialog-paper": { width: "80%" } }}
      maxWidth="xs"
      open={open}
    >
      <DialogTitle>{user ? "ویرایش" : "فرم زیر را پر کنید."}</DialogTitle>
      <DialogContent>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              my: 3,
              gap: 3,
            }}
          >
            <Controller
              name="fullName"
              defaultValue=""
              control={control}
              render={({ field }) => (
                <TextField
                  label="نام و نام خانوادگی"
                  fullWidth
                  {...field}
                  InputLabelProps={{ shrink: true }}
                  error={!!errors.fullName?.message}
                  helperText={errors.fullName?.message}
                />
              )}
            />
            <Controller
              name="mobile"
              defaultValue=""
              control={control}
              render={({ field }) => (
                <TextField
                  label="شماره موبایل"
                  fullWidth
                  {...field}
                  InputLabelProps={{ shrink: true }}
                  error={!!errors.mobile?.message}
                  helperText={errors.mobile?.message}
                />
              )}
            />
            <Controller
              name="age"
              defaultValue=""
              control={control}
              render={({ field }) => (
                <TextField
                  label="سن"
                  fullWidth
                  {...field}
                  InputLabelProps={{ shrink: true }}
                  error={!!errors.age?.message}
                  helperText={errors.age?.message}
                />
              )}
            />
            <Controller
              name="email"
              defaultValue=""
              control={control}
              render={({ field }) => (
                <TextField
                  label="ایمیل"
                  fullWidth
                  {...field}
                  InputLabelProps={{ shrink: true }}
                  error={!!errors.email?.message}
                  helperText={errors.email?.message}
                />
              )}
            />

            {updateUserMutation.isError ? (
              <Alert severity="error">خطایی رخ داده</Alert>
            ) : null}
          </Box>
          <DialogActions>
            <Button variant="contained" color="primary" type="submit">
              ثبت
            </Button>
            <Button onClick={onClose} variant="outlined">
              بستن
            </Button>
          </DialogActions>
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default ItemModal;
